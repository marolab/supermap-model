import React from 'react';
import ReactDOM from 'react-dom';
import MapComponent from './components/MapComponent';

ReactDOM.render(
    <div>
        <MapComponent/>
    </div>,
    document.getElementById('root')
);
