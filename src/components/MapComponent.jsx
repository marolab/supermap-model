import React, { Component } from "react";
import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";
import './MapComponent.css'

export class MapContainer extends Component {

  constructor(props) {
    super(props);
    this.onMarkerClick = this.onMarkerClick.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    this.onChangeLat = this.onChangeLat.bind(this);
    this.onChangeLng = this.onChangeLng.bind(this);

    this.state = {
      showingInfoWindow: true,
      activeMarker: {},
      selectedPlace: {},
      tex: '',
      lat: 0.0,
      lng: 0.0,
    };
  }

  onMarkerClick(props, marker, e) {
    this.setState( {
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    }
    );
  }

  onChangeText(e) {
    this.setState( {tex: e.target.value} )
  }

  onChangeLat(e) {
    this.setState( {lat: e.target.value} )
  }

  onChangeLng(e) {
    this.setState( {lng: e.target.value} )
  }


  render() {
    // const coords = { lat: -29.689193, lng: -51.469312 };
    const coords = { lat: -29.713710, lng: -51.487219};

    if (!this.props.google) {
      return <div>Carregando Syomaps...</div>;
    }

    return (

        <div id="general">
          <form>
            <div>
              <span>
                <h2 id="titulo">Super Map</h2>
                <a style={{ marginLeft:"90%" }}
                   href="https://gitlab.com/marolab/supermap-model/blob/283bbf2d852948367bdcf71f51a43ffa9cc8b740/src/Assets/super-map-help.pdf"
                   target="_blank">About</a>
                <a style={{ marginLeft:"2%" }}
                   href="https://gitlab.com/marolab/supermap-model"
                   target="_blank">Help</a>
              </span>
            </div>
          <div>
              <label class="inx"> Local :
              <input type="text"
                       className="tex"
                       style={{ width: "400px" }}
                       onChange={(e) => this.onChangeText(e)}/>
              </label>
              <label class="inx">Latitude :
                  <input type="number"
                         className="lat"
                         step="any"
                         onChange={(e) => this.onChangeLat(e)}/>
              </label>
              <label class="inx">Longitude :
                  <input type="number"
                         className="lng"
                         step="any"
                         onChange={(e) => this.onChangeLng(e)}/>
              </label>
          <br/>
          </div>
        </form><br/>

        <Map google={this.props.google}
             zoom={15}
             initialCenter={((this.state.lat!=0)?(this.state.lat, this.state.lng):coords)}>
          <Marker

            position={{ lat: this.state.lat, lng: this.state.lng }}
            onClick={this.onMarkerClick}
          />
          <InfoWindow
            marker={this.state.activeMarker}
            visible={this.state.showingInfoWindow}>
            <div>
              <h1>{this.state.tex}</h1>
            </div>
          </InfoWindow>
        </Map>
       </div>
    );
  }
}
export default GoogleApiWrapper({
  apiKey: "AIzaSyDTcp9OSiIf3tnOMocbIAwt3_E2ftIHNkE",
  v: "3.30"
})(MapContainer);

// name={{ tex: this.state.tex }}
// title={{ tex: this.state.tex }}

/**
 *             <label> Texto :
                <input type="text" name="tex" onChange={this.onChange}/>
            </label>
            <label> Latitude :
                <input type="text" name="lat" onChange={this.onChange}/>
            </label>
            <label> Longitude :
                <input type="text" name="lng" onChange={this.onChange}/>
            </label>
 * /////////////
 <label> Texto :
           <input type="text" name="tex" value={this.state.tex} onChange={this.onChange}/>
            </label>
            <label> Latitude :
                <input type="text" name="lat" value={this.state.lat} onChange={this.onChange}/>
            </label>
            <label> Longitude :
                <input type="text" name="lng" value={this.state.lng} onChange={this.onChange}/>
            </label>

 */